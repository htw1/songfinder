
package com.htw.songfinder.models.lastFmModel;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Attr {

    @SerializedName("for")
    private String mFor;


    public void setmFor(String mFor) {
        this.mFor = mFor;
    }

    public String getFor() {
        return mFor;
    }



}
